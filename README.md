The NormSupportEvaluation Java project contains the namargumentationsystem package to implement a norm argument map, along with other files necessary for this goal.

To execute it, with a command line interpreter go to the directory NormSupportEvaluation and type: java FrontEnd

The program has been pre-compiled but to compile it type:
javac FrontEnd.java

The program will show the menu where the user can choose wether to enter data manually or from a file. A test file is provided, so to test the program, the user should choose to enter data from file and when asked to enter the name of the file enter testdata.txt .

To make a loadable file representing a norm argument map, simply follow this guides:

* The first line of the file has all the data for structuring the argumentation in the norm argument map, it has to have 4 values separated by commas in the following order: norm statement, alpha, spectrum lower bound, spectrum upper bound. The norm statement and the spectrum bounds have to be introduced, whereas alpha can be left empty and the default alpha will be used.

* The next lines are for arguments and its opinions, each argument uses a differ- ent line, each argument line will have variable amounts of values separated by commas in the following order: argument type, argument statement, opinion1, opinion2, opinion3, etc. Argument type is either true (argument in favor of the norm) or false (argument agains the norm). For an argument to be considered valid only the type and the statement are necessary, because the number of opinions may vary from none to huge amounts.

Example: Imagine we have a norm argument map debating the norm n, with the default alpha and the spectrum [1, 5]. And we have the positive arguments:

* posarg1 with opinions 1, 2, 4, 5 
* posarg2 with opinions 4, 5, 4, 4, 5

And the negative arguments:

* negarg1 with opinions 1, 2, 1, 3
* negarg2 with opinions 1, 5, 4, 5

Then the text in the file representing this norm argument map would be:

*n,,1,5*

*true,posarg1, 1, 2, 4, 5*

*true,posarg2, 4, 5, 4, 4, 5*

*false,negarg1, 1, 2, 1, 3*

*false,negarg2, 1, 5, 4, 5*

The javadoc for the namargumentationsystem package can be found in NormSupportEvaluation in the doc folder.


This implementation is part of the final research project: Information fusion for norm consensus in virtual communities by Marc Serramià Amorós.