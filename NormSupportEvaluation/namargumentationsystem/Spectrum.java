package namargumentationsystem;
/**
 * This class implements the opinion spectrum of the norm argument map
 * @author Marc
 */
public class Spectrum {
	
	/**
	 * The upper bound of the spectrum
	 */
	private double upperBound;
	/**
	 * The lower bound of the spectrum
	 */
	private double lowerBound;
	
	/**
	 * Creates a new spectrum
	 * @param lb the lower bound of the spectrum
	 * @param ub the upper bound of the spectrum
	 */
	public Spectrum(double lb, double ub) {
		if(lb > ub) {
			double x = ub;
			ub = lb;
			lb = x;
		}
		this.upperBound = ub;
		this.lowerBound = lb;
	}
	
	/**
	 * Compares the spectrum to the one passed as parameter
	 * @param spec the spectrum to compare
	 * @return Boolean true if the spectrums are equal, false otherwise
	 */
	public Boolean equals(Spectrum spec) {
		return ((spec.getLowerBound() == this.lowerBound) && (spec.getUpperBound() == this.upperBound));
	}
	
	/**
	 * Returns the middle point of the spectrum
	 * @return double the middle point of the spectrum ((lb+ub)/2)
	 */
	public double middle() {
		return (this.upperBound+this.lowerBound)/2;
	}
	
	/**
	 * Checks if a number is in the spectrum
	 * @param num the number to check
	 * @return Boolean true if the number is in the spectrum, false otherwise
	 */
	public Boolean has(double num){
		Boolean retorna;
		if(num <= this.upperBound && num >= this.lowerBound) {
			retorna = true;
		} else {
			retorna = false;
		}
		return retorna;
	}
	
	/**
	 * Checks if an opinion value is in the spectrum
	 * @param o the opinion to check
	 * @return Boolean true if the opinion is in the spectrum, false otherwise
	 */
	public Boolean has(Opinion o){
		Boolean retorna;
		if(o.getNum() <= this.upperBound && o.getNum() >= this.lowerBound) {
			retorna = true;
		} else {
			retorna = false;
		}
		return retorna;
	}
	
	/**
	 * Returns the symmetric of a point in the spectrum about the middle of the spectrum
	 * @param x the point the symmetric has to be computed
	 * @return double the point symmetric to x
	 */
	public double symmetric(double x) {
		double result;
		if(this.has(x)){
			result = this.upperBound+this.lowerBound-x;
		} else{
			result = Double.NaN;
		}
		return result;
	}
	
	public String toString() {
		return "["+this.lowerBound+","+this.upperBound+"]";
	}

	public double getUpperBound() {
		return upperBound;
	}

	public void setUpperBound(double upperBound) {
		this.upperBound = upperBound;
	}

	public double getLowerBound() {
		return lowerBound;
	}

	public void setLowerBound(double lowerBound) {
		this.lowerBound = lowerBound;
	}
	
}
