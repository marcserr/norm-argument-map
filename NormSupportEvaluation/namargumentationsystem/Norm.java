package namargumentationsystem;

/**
 * Implements a norm of the norm argument map as a statement
 * @author Marc
 */
public class Norm {
	/**
	 * The statement of the norm
	 */
	private String statement;
	
	/**
	 * Creates a new norm
	 * @param s the norm's statement
	 */
	public Norm(String s) {
		this.statement = s;
	}

	public String getStatement() {
		return statement;
	}

	public void setStatement(String statement) {
		this.statement = statement;
	}
}
