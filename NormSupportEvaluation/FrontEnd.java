
import java.io.*;
import java.util.*;
import namargumentationsystem.*;

public class FrontEnd {
	
	public static void main(String args []) throws FileNotFoundException{
		String option = "";
		NormArgumentMap nam;
		Scanner in = new Scanner(System.in);
		while(!(option.equals("1")) && !(option.equals("2"))){
			System.out.println("Choose an option:\n1.Input data manually\n2.Load a file");
			option = in.nextLine();
		}
		if(option.equals("1")){
			nam = input();
		} else{
			System.out.println("Which file?");
			String filename = in.nextLine();
			File file = new File(filename);
			while(!file.exists()){
				System.err.println("No file named "+filename);
				System.out.println("Which file?");
				filename = in.nextLine();
				file = new File(filename);
			}
			nam = NormArgumentMap.loadFile(file);
		}
		if(nam!=null){
			nam.printStatus(System.out);
		}
		in.close();
	}
	
	public static NormArgumentMap input() { 
		Scanner in = new Scanner(System.in);
		Boolean correct = false;
		double lb=0, ub=0, alpha=0;
		while(!correct) {
			System.out.println("Which is the opinion spectrum lower bound?");
			lb = Double.parseDouble(in.nextLine());
			System.out.println("Which is the opinion spectrum upper bound?");
			ub = Double.parseDouble(in.nextLine());
			if(lb>ub) {
				System.err.println("Lower bound has to be lower than upper bound");
			} else {
				correct = true;
			}
		}
		Spectrum spec = new Spectrum(lb,ub);
		correct = false;
		while(!correct){
			System.out.println("Which alpha?");
			alpha = Double.parseDouble(in.nextLine());
			if(alpha>1 || alpha<0) {
				System.err.println("Alpha has to be in the [0,1] interval");
			} else{
				correct = true;
			}
		}
		System.out.println("Which is the norm?");
		Norm norm = new Norm(in.nextLine());
		NormArgumentMap nam = new NormArgumentMap(norm, alpha, spec);
		inputArgumentSet(nam, true, in, spec);
		inputArgumentSet(nam, false, in , spec);
		return nam;
	}
	
	public static void inputArgumentSet(NormArgumentMap nam, Boolean positive, Scanner in, Spectrum spec) {
		Boolean endArgSet = false, endArg;
		String s;
		double o;
		while(!endArgSet) {
			if(positive) {
				System.out.println("Arguments in favor");
			} else {
				System.out.println("Arguments against");
			}
			System.out.println("------------------\n");
			System.out.println("Which is the statement of the argument? (press Enter to finish entering new positive arguments)");
			s = in.nextLine();
			if(s.isEmpty()){
				endArgSet = true;
			} else {
				Argument arg = new Argument(s, spec, nam.getImportanceFunction());
				nam.addArgument(arg, positive);
				endArg = false;
				while(!endArg) {
					try {
						System.out.println("Enter an opinion (Press Enter to finish entering opinions)");
						o = Double.parseDouble(in.nextLine());
						arg.addOpinion(o);
					} catch (Exception e) {
						endArg = true;
					}
				}
			}
		}
	}
}
